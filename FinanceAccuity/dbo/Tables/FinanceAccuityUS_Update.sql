﻿CREATE TABLE [dbo].[FinanceAccuityUS_Update] (
    [FileKey]                      VARCHAR (17)  NOT NULL,
    [DateUpdated]                  VARCHAR (6)   NULL,
    [TypeOfInstitutionCode]        VARCHAR (2)   NULL,
    [HeadOfficeBranchCodes]        VARCHAR (2)   NULL,
    [RoutingNumber_MICRFormat]     VARCHAR (9)   NULL,
    [RoutingNumber_FractionalForm] VARCHAR (11)  NULL,
    [InstitutionName_Full]         VARCHAR (158) NULL,
    [InstitutionName_Abbreviated]  VARCHAR (50)  NULL,
    [StreetAddress]                VARCHAR (40)  NULL,
    [City]                         VARCHAR (30)  NULL,
    [State]                        VARCHAR (2)   NULL,
    [ZipCode]                      VARCHAR (5)   NULL,
    [Zip4]                         VARCHAR (4)   NULL,
    [MailAddress]                  VARCHAR (40)  NULL,
    [MailCity]                     VARCHAR (30)  NULL,
    [MailState]                    VARCHAR (2)   NULL,
    [MailZipCode]                  VARCHAR (5)   NULL,
    [MailZip4]                     VARCHAR (4)   NULL,
    [BranchOfficeName]             VARCHAR (30)  NULL,
    [HeadOfficeRoutingNumber]      VARCHAR (9)   NULL,
    [PhoneNumber]                  VARCHAR (10)  NULL,
    [PhoneNumberExtension]         VARCHAR (5)   NULL,
    [FaxNumber]                    VARCHAR (10)  NULL,
    [FaxNumberExtension]           VARCHAR (5)   NULL,
    [HeadOfficeAssetSize]          VARCHAR (13)  NULL,
    [FederalReserveDistrictCode]   VARCHAR (6)   NULL,
    [Year2000DateLastUpdated]      VARCHAR (8)   NULL,
    [HeadOfficeFileKey]            VARCHAR (17)  NULL,
    [RoutingNumberType]            VARCHAR (28)  NULL,
    [Filler1_Future]               VARCHAR (7)   NULL,
    [Filler2_Future]               VARCHAR (4)   NULL,
    [RoutingNumberStatus]          VARCHAR (1)   NULL,
    [EmployerTaxID]                VARCHAR (10)  NULL,
    [InstitutionIdentifier]        VARCHAR (8)   NULL,
    [Filler3_Future]               VARCHAR (63)  NULL,
    CONSTRAINT [PK_dbo.FinanceAccuityUS_Update] PRIMARY KEY CLUSTERED ([FileKey] ASC)
);

