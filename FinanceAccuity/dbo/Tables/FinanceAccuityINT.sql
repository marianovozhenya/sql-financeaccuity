﻿CREATE TABLE [dbo].[FinanceAccuityINT] (
    [Key]                   NVARCHAR (40)  NOT NULL,
    [BankTitleAbbr]         NVARCHAR (35)  NOT NULL,
    [ISOCountryCode]        NVARCHAR (2)   NOT NULL,
    [UpdateFlag]            NVARCHAR (1)   NULL,
    [InstitutionType]       NVARCHAR (4)   NULL,
    [OfficeType]            NVARCHAR (4)   NULL,
    [BankTitle]             NVARCHAR (100) NULL,
    [BranchName]            NVARCHAR (35)  NULL,
    [Address]               NVARCHAR (35)  NULL,
    [City]                  NVARCHAR (35)  NULL,
    [Province]              NVARCHAR (35)  NULL,
    [PostalCode]            NVARCHAR (15)  NULL,
    [Country]               NVARCHAR (35)  NULL,
    [Phone]                 NVARCHAR (30)  NULL,
    [Fax]                   NVARCHAR (30)  NULL,
    [MailingAddress]        NVARCHAR (35)  NULL,
    [MailingCity]           NVARCHAR (35)  NULL,
    [MailingProvince]       NVARCHAR (35)  NULL,
    [MailingPostalCode]     NVARCHAR (15)  NULL,
    [MailingISOCountryCode] NVARCHAR (2)   NULL,
    [MailingCountry]        NVARCHAR (35)  NULL,
    [BankCodeType]          NVARCHAR (6)   NULL,
    [BankCode]              NVARCHAR (36)  NULL,
    [SWIFTCode]             NVARCHAR (11)  NULL,
    [CHIPSCode]             NVARCHAR (6)   NULL,
    [Telex]                 NVARCHAR (29)  NULL,
    [AccuityID]             NVARCHAR (8)   NULL,
    [HOAccuityID]           NVARCHAR (9)   NULL,
    CONSTRAINT [PK_dbo.FinanceAccuityINT] PRIMARY KEY CLUSTERED ([Key] ASC, [BankTitleAbbr] ASC, [ISOCountryCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_UpdateFlagAndBankCode]
    ON [dbo].[FinanceAccuityINT]([BankCode] ASC, [UpdateFlag] ASC);

