﻿-- =============================================
-- Author:		Alexandre Morot	
-- Create date: 06/22/2015
-- Description:	Merge the accuity routing table for the US
-- =============================================
CREATE PROCEDURE AccuityMerge_US
AS
BEGIN
DECLARE @err INT

	BEGIN TRAN

	MERGE FinanceAccuityUS AS T
	USING FinanceAccuityUS_Update AS S
	ON (T.FileKey = S.FileKey AND T.RoutingNumber_MICRFormat = S.RoutingNumber_MICRFormat)
	WHEN NOT MATCHED BY TARGET
		THEN INSERT(FileKey, DateUpdated, TypeOfInstitutionCode, HeadOfficeBranchCodes, RoutingNumber_MICRFormat, RoutingNumber_FractionalForm, InstitutionName_Full, InstitutionName_Abbreviated, StreetAddress, City, State, ZipCode, Zip4, MailAddress, MailCity, MailState, MailZipCode, MailZip4, BranchOfficeName, HeadOfficeRoutingNumber, PhoneNumber, PhoneNumberExtension, FaxNumber, FaxNumberExtension, HeadOfficeAssetSize, FederalReserveDistrictCode, Year2000DateLastUpdated, HeadOfficeFileKey, RoutingNumberType, Filler1_Future, Filler2_Future, RoutingNumberStatus, EmployerTaxID, InstitutionIdentifier, Filler3_Future)
			 VALUES(S.FileKey, S.DateUpdated, S.TypeOfInstitutionCode, S.HeadOfficeBranchCodes, S.RoutingNumber_MICRFormat, S.RoutingNumber_FractionalForm, S.InstitutionName_Full, S.InstitutionName_Abbreviated, S.StreetAddress, S.City, S.State, S.ZipCode, S.Zip4, S.MailAddress, S.MailCity, S.MailState, S.MailZipCode, S.MailZip4, S.BranchOfficeName, S.HeadOfficeRoutingNumber, S.PhoneNumber, S.PhoneNumberExtension, S.FaxNumber, S.FaxNumberExtension, S.HeadOfficeAssetSize, S.FederalReserveDistrictCode, S.Year2000DateLastUpdated, S.HeadOfficeFileKey, S.RoutingNumberType, S.Filler1_Future, S.Filler2_Future, S.RoutingNumberStatus, S.EmployerTaxID, S.InstitutionIdentifier, S.Filler3_Future)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	;

	SET @err = @@ERROR
	IF @err <> 0 BEGIN
		PRINT N'An error occurred inserting records into the AccuityRoutingNumbers table.'
		RETURN 99

		ROLLBACK TRAN
	END

	COMMIT TRAN
	PRINT N'Records merged'
END