﻿-- =============================================
-- Author:		Alexandre Morot
-- Create date: 06/22/2015
-- Description:	Merge the Accuity routing table for International
-- =============================================
CREATE PROCEDURE AccuityMerge_INT
AS
BEGIN
DECLARE @err INT

	BEGIN TRAN

	MERGE FinanceAccuityINT AS T
	USING FinanceAccuityINT_Update AS S
	ON (T.[Key] = S.[Key] AND T.[BankCode] = S.[BankCode])
	WHEN NOT MATCHED BY TARGET
		THEN INSERT([UpdateFlag], [Key], [InstitutionType], [OfficeType], [BankTitleAbbr], [BankTitle], [BranchName], [Address], [City], [Province], [PostalCode], [ISOCountryCode], [Country], [Phone], [Fax], [MailingAddress], [MailingCity], [MailingProvince], [MailingPostalCode], [MailingISOCountryCode], [MailingCountry], [BankCodeType], [BankCode], [SWIFTCode], [CHIPSCode], [Telex], [AccuityID], [HOAccuityID])
			 VALUES(S.[UpdateFlag], S.[Key], S.[InstitutionType], S.[OfficeType], S.[BankTitleAbbr], S.[BankTitle], S.[BranchName], S.[Address], S.[City], S.[Province], S.[PostalCode], S.[ISOCountryCode], S.[Country], S.[Phone], S.[Fax], S.[MailingAddress], S.[MailingCity], S.[MailingProvince], S.[MailingPostalCode], S.[MailingISOCountryCode], S.[MailingCountry], S.[BankCodeType], S.[BankCode], S.[SWIFTCode], S.[CHIPSCode], S.[Telex], S.[AccuityID], S.[HOAccuityID])
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	;

	SET @err = @@ERROR
	IF @err <> 0 BEGIN
		PRINT N'An error occurred inserting records into the AccuityRoutingNumbers table.'
		RETURN 99

		ROLLBACK TRAN
	END

	COMMIT TRAN
	PRINT N'Records merged'
END